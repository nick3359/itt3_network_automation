
from netmiko import ConnectHandler

juniper_vsrx1 = {
        'device_type': 'juniper',
        'ip':   '10.10.10.1',
        'username': 'root',
        'password': 'Testtest',
        #'port': 9822,               # there is a firewall performing NAT in front of this $
        'verbose': False,
}

juniper_vsrx2 = {
        'device_type': 'juniper',
        'ip': '11.11.11.1',
        'username': 'root',
        'password': "Testtest",
        'verbose': False,
}

all_devices = [juniper_vsrx1, juniper_vsrx2]

start_time = datetime.now()

for a_device in all_devices:
        net_connect = ConnectHandler(**a_device)
        output = net_connect.send_command("show configuration")

        print output

        with open (a_device['ip'] + '_config.json', 'w') as file:
                file.write(output)

        print "Output is written to a file"


